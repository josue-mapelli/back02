package com.jmapelli.back01.dto;

public class Account {

    public String number;
    public String currency;
    public String balance;
    public String type;
    public String status;
    public String branch;

}

