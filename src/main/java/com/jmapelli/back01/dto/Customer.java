package com.jmapelli.back01.dto;

public class Customer {

    public String dni;
    public String fullname;
    public String age;
    public String birthdate;
    public String phone;
    public String email;
    public String address;

}
