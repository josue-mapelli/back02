package com.jmapelli.back01.mapper;

import com.jmapelli.back01.dto.Account;
import com.jmapelli.back01.repository.model.AccountModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountMapper {

    public List<Account> map(final List<AccountModel> models) {
        return models.stream().map(this::map).collect(Collectors.toList());
    }

    public Account map(final AccountModel model) {
        Account account = new Account();

        account.number = model.getNumber();
        account.currency = model.getCurrency();
        account.balance = model.getBalance();
        account.type = model.getType();
        account.status = model.getStatus();
        account.branch = model.getBranch();

        return account;
    }

    public AccountModel map(final String dni, final Account account) {
        AccountModel model = new AccountModel();

        model.setNumber(account.number);
        model.setCurrency(account.currency);
        model.setBalance(account.balance);
        model.setType(account.type);
        model.setStatus(account.status);
        model.setBranch(account.branch);
        model.setCustomer(dni);

        return model;
    }

}
