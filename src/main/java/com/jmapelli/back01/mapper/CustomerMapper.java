package com.jmapelli.back01.mapper;

import com.jmapelli.back01.dto.Customer;
import com.jmapelli.back01.repository.model.CustomerModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerMapper {

    public List<Customer> map(final List<CustomerModel> models) {
        return models.stream().map(this::map).collect(Collectors.toList());
    }

    public Customer map(final CustomerModel model) {
        Customer customer = new Customer();

        customer.dni = model.getDni();
        customer.fullname = model.getFullname();
        customer.age = model.getAge();
        customer.birthdate = model.getBirthdate();
        customer.phone = model.getPhone();
        customer.email = model.getEmail();
        customer.address = model.getAddress();

        return customer;
    }

    public CustomerModel map(final Customer customer) {
        CustomerModel model = new CustomerModel();

        model.setDni(customer.dni);
        model.setFullname(customer.fullname);
        model.setAge(customer.age);
        model.setBirthdate(customer.birthdate);
        model.setPhone(customer.phone);
        model.setEmail(customer.email);
        model.setAddress(customer.address);

        return model;
    }

}
