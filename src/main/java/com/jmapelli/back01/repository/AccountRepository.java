package com.jmapelli.back01.repository;

import com.jmapelli.back01.repository.model.AccountModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AccountRepository extends MongoRepository<AccountModel, String> {

    List<AccountModel> findByCustomer(final String dni);

}
