package com.jmapelli.back01.repository;

import com.jmapelli.back01.repository.model.CustomerModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<CustomerModel, String> {
}
