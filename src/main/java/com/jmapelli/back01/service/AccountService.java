package com.jmapelli.back01.service;

import com.jmapelli.back01.dto.Account;

import java.util.List;

public interface AccountService {

    List<Account> listAccounts(final String dni);

    void saveAccount(final String dni, final Account account);

    Account getAccount(final String dni, final String accountNumber);

    void updateAccount(final String dni, final String accountNumber, final Account account);

    void patchAccount(final String dni, final String accountNumber, final Account source, final Account target);

    void deleteAccount(final String accountNumber);

}
