package com.jmapelli.back01.service;

import com.jmapelli.back01.dto.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> listCustomer();

    void saveCustomer(final Customer customer);

    Customer getCustomer(final String dni);

    void updateCustomer(final String dni, final Customer customer);

    void patchCustomer(final String dni, final Customer customer, Customer target);

    void deleteCustomer(final String dni);

}
