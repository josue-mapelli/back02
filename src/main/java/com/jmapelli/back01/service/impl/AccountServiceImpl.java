package com.jmapelli.back01.service.impl;

import com.jmapelli.back01.dto.Account;
import com.jmapelli.back01.mapper.AccountMapper;
import com.jmapelli.back01.repository.AccountRepository;
import com.jmapelli.back01.repository.model.AccountModel;
import com.jmapelli.back01.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Account> listAccounts(String dni) {
        List<AccountModel> models = this.accountRepository.findByCustomer(dni);

        if (Objects.isNull(models)) {
            return Collections.emptyList();
        }

        return this.accountMapper.map(models);
    }

    @Override
    public void saveAccount(String dni, Account account) {
        AccountModel model = this.accountMapper.map(dni, account);
        this.accountRepository.save(model);
    }

    @Override
    public Account getAccount(String dni, String accountNumber) {
        Optional<AccountModel> model = this.accountRepository.findById(accountNumber);

        if (model.isPresent()) {
            return this.accountMapper.map(model.get());
        }

        return null;
    }

    @Override
    public void updateAccount(String dni, String accountNumber, Account account) {
        account.number = accountNumber;
        this.saveAccount(dni, account);
    }

    @Override
    public void patchAccount(String dni, String accountNumber, Account source, Account target) {
        if (Objects.nonNull(source.status)) {
            target.status = source.status;
        }

        if (Objects.nonNull(source.balance)) {
            target.balance = source.balance;
        }

        this.updateAccount(dni, accountNumber, target);
    }

    @Override
    public void deleteAccount(String accountNumber) {
        this.accountRepository.deleteById(accountNumber);
    }


}
