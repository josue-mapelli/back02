package com.jmapelli.back01.service.impl;

import com.jmapelli.back01.dto.Customer;
import com.jmapelli.back01.mapper.CustomerMapper;
import com.jmapelli.back01.repository.CustomerRepository;
import com.jmapelli.back01.repository.model.CustomerModel;
import com.jmapelli.back01.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public List<Customer> listCustomer() {
        List<CustomerModel> models = this.customerRepository.findAll();
        return this.customerMapper.map(models);
    }

    @Override
    public void saveCustomer(Customer customer) {
        CustomerModel customerModel = this.customerMapper.map(customer);
        this.customerRepository.save(customerModel);
    }

    @Override
    public Customer getCustomer(String dni) {
        Optional<CustomerModel> customerModel = this.customerRepository.findById(dni);

        if (customerModel.isPresent()) {
            return this.customerMapper.map(customerModel.get());
        }

        return null;
    }

    @Override
    public void updateCustomer(String dni, Customer source) {
        source.dni = dni;

        this.saveCustomer(source);
    }

    @Override
    public void patchCustomer(String dni, Customer source, Customer target) {
        if (Objects.nonNull(source.fullname)) {
            target.fullname = source.fullname;
        }

        if (Objects.nonNull(source.age)) {
            target.age = source.age;
        }

        if (Objects.nonNull(source.birthdate)) {
            target.birthdate = source.birthdate;
        }

        if (Objects.nonNull(source.phone)) {
            target.phone = source.phone;
        }

        if (Objects.nonNull(source.email)) {
            target.email = source.email;
        }

        if (Objects.nonNull(source.address)) {
            target.address = source.address;
        }

        this.saveCustomer(target);
    }

    @Override
    public void deleteCustomer(String dni) {
        this.customerRepository.deleteById(dni);
    }

}
